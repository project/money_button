<?php

$plugin = array(
  'title' => t('Money button'),
  'description' => t('Display our money button'),
  'single' => TRUE,
  'edit form' => 'money_button_plugin_content_type_edit_form',
  'render callback' => 'money_button_plugin_content_type_render',
  'category' => t('Money button'),
);
/*
 * render the money button
 */
function money_button_plugin_content_type_render($subtype, $conf, $panel_args, $context) {
  // Define the return block.
  $block = new stdClass();
  $block->content = theme('money_button',array('config' => money_button_get_parameters()));
  return $block;
}
/**
 * Enable admin settings page.
 * could be interesting to config from here ?
 */
function money_button_plugin_content_type_edit_form($form, &$form_state) {
  return $form;
}